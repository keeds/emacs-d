;;; keeds-babel.el

;; org babel - clojure
;; (add-to-list 'load-path "~/elisp/org-mode/lisp")

(require 'cider)
(setq org-babel-clojure-backend 'cider)

(require 'ob-clojure)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((clojure . t)
   (gnuplot . t)))


(provide 'keeds-babel)

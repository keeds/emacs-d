;;; queue-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "queue" "queue.el" (21637 36692 734424 897000))
;;; Generated autoloads from queue.el

(defalias 'make-queue 'queue-create "\
Create an empty queue data structure.")

;;;***

;;;### (autoloads nil nil ("queue-pkg.el") (21637 36692 752819 855000))

;;;***

(provide 'queue-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; queue-autoloads.el ends here

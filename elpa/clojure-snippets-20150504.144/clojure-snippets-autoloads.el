;;; clojure-snippets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "clojure-snippets" "clojure-snippets.el" (21936
;;;;;;  56165 289149 613000))
;;; Generated autoloads from clojure-snippets.el

(autoload 'clojure-snippets-initialize "clojure-snippets" "\


\(fn)" nil nil)

(eval-after-load 'yasnippet '(clojure-snippets-initialize))

;;;***

;;;### (autoloads nil nil ("clojure-snippets-pkg.el") (21936 56165
;;;;;;  297411 582000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; clojure-snippets-autoloads.el ends here

(define-package "inf-clojure" "20151129.34" "an inferior-clojure mode" '((emacs "24.1") (clojure-mode "5.0")) :url "http://github.com/clojure-emacs/inf-clojure" :keywords '("processes" "clojure"))

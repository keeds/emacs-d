(setq inhibit-startup-screen t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(blink-cursor-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)

;; (set-face-font 'default "-adobe-Source Code Pro-normal-normal-normal-*-*-*-*-*-m-0-iso10646-1")
;; (set-face-attribute 'default nil :height 150)

(global-visual-line-mode t)

(add-to-list 'load-path "~/elisp/org-mode/lisp")
(add-to-list 'load-path "~/.emacs.d/keeds")

(require 'package)
(add-to-list 'package-archives
	     '("melpa" .
	       "http://melpa.milkbox.net/packages/") t)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(defun maybe-install-and-require (p)
  (when (not (package-installed-p p))
    (package-install p))
  (require p))

(maybe-install-and-require 'diminish)

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward
      uniquify-separator ":"
      uniquify-after-kill-buffer-p t
      uniquify-ignore-buffers-re "^\\*")

(defvar my-packages
  '(
    dash
    clojure-mode
    cider
    company
    pkg-info
    gist
    eldoc
    paredit
    magit
    zenburn-theme
    git-gutter
    undo-tree
    cl-lib
    rainbow-delimiters
    highlight-parentheses
    powerline
    haskell-mode
    markdown-mode
    psci
    projectile
    helm
    golden-ratio
    yasnippet
    clojure-snippets
    diminish
    js2-mode
    coffee-mode
    ))

(dolist (p my-packages)
  (when (not (package-installed-p p)) (package-install p)))

(global-hl-line-mode 1)

;; magit
(global-set-key (kbd "C-c C-g") 'magit-status)
(global-set-key (kbd "C-c C-b") 'magit-blame-mode)

;; theme
(load-theme 'zenburn t)

;; ido
;; (require 'ido)
;; (ido-mode t)

;; scrolling
(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-preserve-screen-position 1)

;; line numbers  
(line-number-mode t)
(column-number-mode t)
(size-indication-mode t)

;; undo-tree
(require 'undo-tree)
(diminish 'undo-tree-mode "Ut")
(global-undo-tree-mode 1)

;; git-gutter
(require 'git-gutter)
(diminish 'git-gutter-mode "Gg")
(global-git-gutter-mode t)

;; parens
(require 'rainbow-delimiters)
(require 'highlight-parentheses)

;; (defadvice ido-find-file (after find-file-sudo activate)
;;   "Find file as root if necessary."
;;   (unless (and buffer-file-name
;;                (file-writable-p buffer-file-name))
;;     (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

;; ;; Add .emacsd to load-path
;; (setq dotfiles-dir (file-name-directory
;; 		    (or (buffer-file-name) load-file-name)))
;; (add-to-list 'load-path dotfiles-dir)


(defun sudo-edit (&optional arg)
  "Edit currently visited file as root.
   With a prefix ARG prompt for a file to visit.
   Will also prompt for a file to visit if current
   buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(global-set-key (kbd "C-x C-r") 'sudo-edit)

;; load keeds packages
(setq keeds-pkg
      '(
	keeds-cider
	keeds-helm
	;; keeds-babel
	;; keeds-clojureinf
	;; keeds-theme
	;; keeds-osx
	;; keeds-haskell
	;; keeds-org
	))

(dolist (file keeds-pkg)
  (require file))

(maybe-install-and-require 'smartscan)
(smartscan-mode 1)

(maybe-install-and-require 'ace-window)
(global-set-key (kbd "M-m") 'ace-window)

(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))

;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(custom-safe-themes
;;    (quote
;;     ("c6533322a2721eb57be71a26a966a43eeb5a68ec0c132c7c064b7f4b7e523e6e" "de538b2d1282b23ca41ac5d8b69c033b911521fe27b5e1f59783d2eb20384e1f" default)))
;;  '(org-agenda-files (quote ("~/Dropbox/org/keeds.org" "~/Dropbox/org/bom.org")))
;;  '(safe-local-variable-values
;;    (quote
;;     ((haskell-process-use-ghci . t)
;;      (haskell-indent-spaces . 4)
;;      (org-confirm-babel-evaluate)))))

;; ;; (custom-set-faces
;; ;;  ;; custom-set-faces was added by Custom.
;; ;;  ;; If you edit it by hand, you could mess it up, so be careful.
;; ;;  ;; Your init file should contain only one such instance.
;; ;;  ;; If there is more than one, they won't work right.
;; ;;  )
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )
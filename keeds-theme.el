;;; keeds-theme.el

(require 'powerline)
(powerline-default-theme)

(provide 'keeds-theme)

;;; keeds-haskell.el


(maybe-install-and-require 'haskell-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;; (eval-after-load "haskell-mode"
;;   '(define-key haskell-mode-map (kbd "C-c C-c") 'haskell-compile))

(maybe-install-and-require 'ghc)
(autoload 'ghc-init "ghc" nil t)
(autoload 'ghc-debug "ghc" nil t)
(add-hook 'haskell-mode-hook (lambda () (ghc-init)))

(provide 'keeds-haskell)

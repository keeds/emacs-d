;;; keeds-cider.el

;; cider  - clojure

(require 'clojure-mode)
;; (diminish 'clojure-mode "Clj")
(setq auto-mode-alist (cons '("\\.cljs$" . clojure-mode) auto-mode-alist))

;; clojure-mode
;; (setq clojure-defun-style-default-indent t)

;; hook
(add-hook 'clojure-mode-hook 'paredit-mode)
(add-hook 'emacs-lisp-mode-hook 'paredit-mode)
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
(add-hook 'clojure-mode-hook 'highlight-parentheses-mode)

(require 'paredit)
(diminish 'paredit-mode "Pe")
(add-hook 'clojure-mode-hook 'paredit-mode)

(require 'projectile)
(diminish 'projectile-mode "Prj")
(add-hook 'clojure-mode-hook 'projectile-mode)

(require 'company)
(diminish 'company-mode "Cmpy")
(add-hook 'after-init-hook 'global-company-mode)

(require 'company-etags)
(add-to-list 'company-etags-modes 'clojure-mode)

;; (require 'golden-ratio)
;; (golden-ratio-enable)

(require 'cider)
(require 'eldoc)

(global-company-mode)

;; (require 'popup)

;; (require 'auto-complete)
;; (require 'auto-complete-config)
;; (ac-config-default)

;; (global-auto-complete-mode t)
;; (setq ac-auto-show-menu t)
;; (setq ac-dwim t)
;; (setq ac-use-menu-map t)
;; (setq ac-quick-help-delay 1)
;; (setq ac-quick-help-height 60)
;; (setq ac-disable-inline t)
;; (setq ac-show-menu-immediately-on-auto-complete t)
;; (setq ac-auto-start 2)
;; (setq ac-candidate-menu-min 0)

;; (dolist (mode '(clojure-mode))
;;   (add-to-list 'ac-modes mode))


;; (require 'ac-nrepl)
;; (require 'ac-cider-compliment)
;; (add-hook 'cider-mode-hook 'ac-flyspell-workaround)
;; (add-hook 'cider-mode-hook 'ac-cider-compliment-setup)
;; (add-hook 'cider-repl-mode-hook 'ac-cider-compliment-repl-setup)
;; (eval-after-load "auto-complete"
;;   '(add-to-list 'ac-modes cider-mode))

;; (add-hook 'cider-mode-hook 'ac-nrepl-setup)
;; (add-hook 'cider-repl-mode-hook 'ac-nrepl-setup)

;; (eval-after-load "auto-complete"
;;   '(add-to-list 'ac-modes 'cider-mode))

(add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
(setq nrepl-hide-special-buffers t)
(setq cider-repl-pop-to-buffer-on-connect nil)
;; (setq cider-popup-stacktraces nil)
(setq cider-repl-print-length 100) ; the default is nil, no limit

(setq cider-repl-wrap-history t)
(setq cider-repl-history-size 1000) ; the default is 500
(setq cider-repl-history-file "~/.emacs.d/cider-history")


(add-hook 'cider-repl-mode-hook 'paredit-mode)
(add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode)

;; (eval-after-load "cider"
;;   '(define-key cider-mode-map (kbd "C-c C-d") 'ac-cider-compliment-popup-doc))

;; (eval-after-load "cider"
;;   '(define-key cider-repl-mode-map (kbd "C-c C-d") 'ac-cider-compliment-popup-doc))

;; (require 'company)
;; (add-hook 'after-init-hook 'global-company-mode)


(defun nrepl-reset ()
  (interactive)
  (save-some-buffers)
  (cider-switch-to-repl-buffer)
  (goto-char (point-max))
  (insert "(user/reset)")
  (cider-repl-return))

(global-set-key (kbd "C-c r") 'nrepl-reset)

(provide 'keeds-cider)

;;; keeds-helm.el

(maybe-install-and-require 'helm)
(maybe-install-and-require 'helm-ag)
(maybe-install-and-require 'helm-projectile)
(maybe-install-and-require 'helm-swoop)
(maybe-install-and-require 'helm-themes)

(require 'helm-config)
(helm-mode 1)
(helm-adaptative-mode 1)
(helm-autoresize-mode 1)
(helm-push-mark-mode 1)

;;; Helm-command-map
;;
;;
(define-key helm-command-map (kbd "g")   'helm-apt)
(define-key helm-command-map (kbd "w")   'helm-psession)
(define-key helm-command-map (kbd "z")   'helm-complex-command-history)
(define-key helm-command-map (kbd "w")   'helm-w3m-bookmarks)
(define-key helm-command-map (kbd "x")   'helm-firefox-bookmarks)
(define-key helm-command-map (kbd "#")   'helm-emms)
(define-key helm-command-map (kbd "I")   'helm-imenu-in-all-buffers)

;;; Global-map
;;
;;
(global-set-key (kbd "M-x")                          'undefined)
(global-set-key (kbd "M-x")                          'helm-M-x)
(global-set-key (kbd "M-y")                          'helm-show-kill-ring)
(global-set-key (kbd "C-c f")                        'helm-recentf)
(global-set-key (kbd "C-x C-f")                      'helm-find-files)
(global-set-key (kbd "C-c <SPC>")                    'helm-all-mark-rings)
(global-set-key (kbd "C-x r b")                      'helm-filtered-bookmarks)
(global-set-key (kbd "C-h r")                        'helm-info-emacs)
(global-set-key (kbd "C-:")                          'helm-eval-expression-with-eldoc)
(global-set-key (kbd "C-,")                          'helm-calcul-expression)
(global-set-key (kbd "C-h i")                        'helm-info-at-point)
(global-set-key (kbd "C-x C-d")                      'helm-browse-project)
(global-set-key (kbd "<f1>")                         'helm-resume)
(global-set-key (kbd "C-h C-f")                      'helm-apropos)
(global-set-key (kbd "<f5> s")                       'helm-find)
(global-set-key (kbd "<f2>")                         'helm-execute-kmacro)
(global-set-key (kbd "C-c g")                        'helm-gid)
(global-set-key (kbd "C-c i")                        'helm-imenu-in-all-buffers)
(define-key global-map [remap jump-to-register]      'helm-register)
(define-key global-map [remap list-buffers]          'helm-buffers-list)
(define-key global-map [remap dabbrev-expand]        'helm-dabbrev)
(define-key global-map [remap find-tag]              'helm-etags-select)
(define-key global-map [remap xref-find-definitions] 'helm-etags-select)
(define-key shell-mode-map (kbd "M-p")               'helm-comint-input-ring)

;;; Helm-map
;;
;;
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)

;;; Helm-variables
;;
;;

(setq helm-scroll-amount 4
      helm-ff-smart-completion t
      helm-ff-skip-boring-files t)


;; Previous config

;; (when (executable-find "curl")
;;   (setq helm-google-suggest-use-curl-p t))

;; ;; See https://github.com/bbatsov/prelude/pull/670 for a detailed
;; ;; discussion of these options.
;; (setq helm-split-window-in-side-p           t
;;       helm-buffers-fuzzy-matching           t
;;       helm-move-to-line-cycle-in-source     t
;;       helm-ff-search-library-in-sexp        t
;;       helm-ff-file-name-history-use-recentf t)

;; ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
;; (global-set-key (kbd "C-c h") 'helm-command-prefix)
;; (global-unset-key (kbd "C-x c"))

;; (define-key helm-command-map (kbd "o")     'helm-occur)
;; (define-key helm-command-map (kbd "g")     'helm-do-grep)
;; (define-key helm-command-map (kbd "C-c w") 'helm-wikipedia-suggest)
;; (define-key helm-command-map (kbd "SPC")   'helm-all-mark-rings)

;; (global-set-key (kbd "M-x") 'helm-M-x)
;; (global-set-key (kbd "C-x C-m") 'helm-M-x)
;; (global-set-key (kbd "M-y") 'helm-show-kill-ring)
;; (global-set-key (kbd "C-x b") 'helm-mini)
;; (global-set-key (kbd "C-x C-b") 'helm-buffers-list)
;; (global-set-key (kbd "C-x C-f") 'helm-find-files)
;; (global-set-key (kbd "C-h f") 'helm-apropos)
;; (global-set-key (kbd "C-h r") 'helm-info-emacs)
;; (global-set-key (kbd "C-h C-l") 'helm-locate-library)
;; ;; (define-key prelude-mode-map (kbd "C-c f") 'helm-recentf)

;; (define-key minibuffer-local-map (kbd "C-c C-l") 'helm-minibuffer-history)

;; ;; shell history.
;; (define-key shell-mode-map (kbd "C-c C-l") 'helm-comint-input-ring)

;; ;; use helm to list eshell history
;; (add-hook 'eshell-mode-hook
;;           #'(lambda ()
;;               (substitute-key-definition 'eshell-list-history 'helm-eshell-history eshell-mode-map)))

;; (substitute-key-definition 'find-tag 'helm-etags-select global-map)
;; (setq projectile-completion-system 'helm)
;; ;; (helm-descbinds-mode)
;; (helm-mode 1)

;; ;; enable Helm version of Projectile with replacment commands
;; (require 'helm-projectile)
;; ;; (helm-projectile)

;; (defun helm-clojure-headlines ()
;;   "Display headlines for the current Clojure file."
;;   (interactive)
;;   (helm :sources '(((name . "Clojure Headlines")
;;                     (volatile)
;;                     (headline "^[;(]")))))

(provide 'keeds-helm)

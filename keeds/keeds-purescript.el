;;; keeds-purescript.el

;; purescript

(require 'purescript-mode)

(add-hook 'purescript-mode-hook 'purescript-hook)

(defun purescript-hook ()
  (turn-on-purescript-simple-indent))


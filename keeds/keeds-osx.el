;;; keeds-osx.el

;; mac osx pound sign
(global-set-key (kbd "M-3") (lambda () (interactive) (insert "#")))
(define-key global-map (kbd "C-c 3") (lambda () (interactive) (insert "#")))

(provide 'keeds-osx)
